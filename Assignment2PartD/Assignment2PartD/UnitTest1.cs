﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Assignment2PartD
{
    [TestClass]
    public class UnitTest1
    {
        //read sample regions from text file and return list of strings
        public List<String> readSamples(String path)
        {
            List<String> sampleList = new List<string>();
            string[] lines = System.IO.File.ReadAllLines(path);
            String currentDistricts = "";

            foreach (string line in lines)
            {
                if (line == "") //empty line between regions, add district string to list of regions
                {
                    sampleList.Add(currentDistricts);
                    currentDistricts = ""; 
                    continue;
                }
                if (line == "--") continue; // signifying next line is a district
                currentDistricts += line + ","; // appends to region string
            }
            sampleList.Add(currentDistricts); // appends last region
            return sampleList;
        }// end of readSamples method

        //Test correctness of region and district values for realestate.co.nz
        [TestMethod]
        public void TestMethod1()
        {
            //set up
            List<String> sampleList = readSamples("C:\\Users\\adric\\source\\repos\\Assignment2\\Assignment2PartD\\Assignment2PartD\\SampleRegionList.txt");
            IWebDriver driver = new ChromeDriver();
            //navigate to realestate, then the classic site
            driver.Navigate().GoToUrl("http://www.realestate.co.nz");
            driver.FindElement(By.XPath("//*[@id=\"ember287\"]/header/div/div[2]/div/nav/div[4]/div[2]")).Click();//opens dropdown 
            driver.FindElement(By.XPath("//*[@id=\"ember287\"]/header/div/div[2]/div/nav/div[4]/div[2]/ul/li[3]/a")).Click();//opens classic site

            List<String> actualList = new List<String>();
            String currentDistricts = ""; //string to handle districts during extraction

            IWebElement regionElement = driver.FindElement(By.XPath("//*[@id=\"search_filters_regions\"]")); //region dropdown
            SelectElement region = new SelectElement(regionElement);

            IWebElement districtElement = driver.FindElement(By.XPath("//*[@id=\"search_filters_districts\"]")); // district dropdown
            SelectElement district = new SelectElement(districtElement);
            
            //loop through regions, appending each region and its districts to a string, add to the list
            foreach (IWebElement selectionRegion in region.Options)
            {
                if (selectionRegion.Text == "All Regions") continue; //ignore initial value
                region.SelectByText(selectionRegion.Text);
                currentDistricts += selectionRegion.Text+",";
                foreach(IWebElement selectionDistrict in district.Options)
                {
                    if (selectionDistrict.Text == "All Districts") continue; // ignore initial value
                    currentDistricts +=selectionDistrict.Text+ ",";
                }
                actualList.Add(currentDistricts);
                currentDistricts = "";
            }
            CollectionAssert.AreEqual(actualList , sampleList);
        }// end of TestMethod1
    }//end of UnitTest1
}//end of namespace
