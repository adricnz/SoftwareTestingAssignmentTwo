﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Assignment2PartA
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        //test  calculator.net for cos equation
        public void TestMethod1()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://www.calculator.net");
            //find button elements and click
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[2]/span[2]")).Click(); // 5
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[4]/span[1]")).Click();// 0
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[1]/div/div[1]/span[2]")).Click();// cos

            //get the answer, convert to double and compare to expected
            IWebElement resultText = driver.FindElement(By.XPath("//*[@id=\"sciOutPut\"]"));

            double actual = double.Parse(resultText.Text);
            double expected = 0.6427876097;

            Assert.AreEqual(actual, expected, 0.00001, "Incorrect result when passed 50 cos");

        }// end of TestMethod1

        [TestMethod]
        //test  calculator.net for log equation
        public void TestMethod2()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://www.calculator.net");
            //find button elements and click
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[3]/span[1]")).Click(); // 1
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[4]/span[1]")).Click(); // 0
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[4]/span[1]")).Click(); // 0
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[1]/div/div[4]/span[5]")).Click(); // log

            //get the answer, convert to double and compare to expected
            IWebElement resultText = driver.FindElement(By.XPath("//*[@id=\"sciOutPut\"]"));

            double actual = double.Parse(resultText.Text);
            double expected = 2;

            Assert.AreEqual(actual, expected, 0.00001, "Incorrect result when passed 100 log");
        }//end of TestMethod2

        [TestMethod]
        //test calculator.net for log equation
        public void TestMethod3()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://www.calculator.net");
            //find button elements and click
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[3]/span[1]")).Click(); // 1
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[4]/span[1]")).Click(); // 0
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[1]/div/div[3]/span[1]")).Click(); // ^
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[3]/span[2]")).Click(); // 2
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[5]/span[4]")).Click(); // =

            //get the answer, convert to double and compare to expected
            IWebElement resultText = driver.FindElement(By.XPath("//*[@id=\"sciOutPut\"]"));

            double actual = double.Parse(resultText.Text);
            double expected = 100;

            Assert.AreEqual(actual, expected, 0.00001, "Incorrect result when passed 10 ^ 2");
        } //end of TestMethod3

        [TestMethod]
        //tests calculator.net for n factorial equation
        public void TestMethod4()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://www.calculator.net");
            //find button elements and click
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[2]/div/div[2]/span[2]")).Click(); // 5
            driver.FindElement(By.XPath("//*[@id=\"sciout\"]/tbody/tr[2]/td[1]/div/div[5]/span[5]")).Click(); // factorial

            //get the answer, convert to double and compare to expected
            IWebElement resultText = driver.FindElement(By.XPath("//*[@id=\"sciOutPut\"]"));

            double actual = double.Parse(resultText.Text);
            double expected = 120;

            Assert.AreEqual(actual, expected, 0.00001, "Incorrect result when passed 5!");

        }// end of TestMethod4

    }// end of class UnitTest1
}//end of namespace
