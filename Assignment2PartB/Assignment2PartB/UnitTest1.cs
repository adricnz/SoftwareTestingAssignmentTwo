﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Text;

namespace Assignment2PartB
{
    [TestClass]
    public class UnitTest1
    {
        //verifies the correctness of the amount to be paid when purchasing three items
        [TestMethod]
        public void TestMethod1()
        {
            double priceOne, priceTwo, priceThree, expectedTotal, actualTotal;
            //Initialises a new instance of chromedriver class
            IWebDriver driver = new ChromeDriver();

            //open automationpractice.com
            driver.Navigate().GoToUrl("http://automationpractice.com");

            //find add to cart element for faded short sleeve tshirt and click. assign to priceOne
            driver.FindElement(By.XPath("//*[@id=\"homefeatured\"]/li[1]/div/div[2]/div[2]/a[1]")).Click();
            priceOne = double.Parse(driver.FindElement(By.XPath("//*[@id=\"homefeatured\"]/li[1]/div/div[2]/div[1]/span")).Text.Remove(0, 1));

            //ensure the continue shopping element has loaded
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span")));

            //find continue shopping element and click
            driver.FindElement(By.XPath("//*[@id=\"layer_cart\"]/div[1]/div[1]/span")).Click();

            //wait until popup has deloaded, find add to cart element for Blouse and click, assign to priceTwo
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//*[@id=\"layer_cart\"]/div[1]/div[1]/span")));
            driver.FindElement(By.XPath("//*[@id=\"homefeatured\"]/li[2]/div/div[2]/div[2]/a[1]")).Click();
            priceTwo = double.Parse(driver.FindElement(By.XPath("//*[@id=\"homefeatured\"]/li[2]/div/div[2]/div[1]/span")).Text.Remove(0, 1));

            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span")));
            //find continue shopping element and click
            driver.FindElement(By.XPath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span")).Click();

            //wait until popup has deloaded and find add to cart element for Printed Dress and click, assign to priceThree
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.XPath("//*[@id=\"layer_cart\"]/div[1]/div[1]/span")));
            driver.FindElement(By.XPath("//*[@id=\"homefeatured\"]/li[3]/div/div[2]/div[2]/a[1]")).Click();
            priceThree = double.Parse(driver.FindElement(By.XPath("//*[@id=\"homefeatured\"]/li[3]/div/div[2]/div[1]/span")).Text.Remove(0, 1));

            //wait until popup has loaded
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span")));

            //find checkout element and click
            driver.FindElement(By.XPath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a")).Click();

            //get total product price, calculate expected
            IWebElement resultText = driver.FindElement(By.XPath("//*[@id=\"total_product\"]"));

            expectedTotal = priceOne + priceTwo + priceThree;
            actualTotal = double.Parse(resultText.Text.Remove(0,1));// removes $


            Assert.AreEqual(expectedTotal, actualTotal, 0.00001, "Incorrect total price");

        }//end of TestMethod1
    }// end of UnitTest1
}// end of namespace
