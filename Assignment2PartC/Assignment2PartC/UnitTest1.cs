﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Text;
using System.Collections.Generic;
using System.Net;


namespace Assignment2PartC
{

    /*Write a unit test that validates the availability of all 
    links in this page
    */
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TestMethod1()
        {

            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://www.automationpractice.com");

            IList<IWebElement> anchors = driver.FindElements(By.TagName("a")); // get list of anchor links
            IList<IWebElement> imgs = driver.FindElements(By.TagName("img")); // get list of anchor links
            IList<IWebElement> links = new List<IWebElement>(); // creating list to populate with hrefs

            // loop through anchors and imgs, if href is present and link exists, append to links list
            foreach (IWebElement el in anchors)
            {
                if (!String.IsNullOrEmpty(el.GetAttribute("href")))
                {
                    links.Add(el);
                }
            }

            foreach (IWebElement el in imgs)
            {
                if (!String.IsNullOrEmpty(el.GetAttribute("href")))
                {
                    links.Add(el);

                }
            }

            //loop through final list, trying each link. appends text and URL to failedList if invalid URL
            HttpWebRequest req;
            HttpWebResponse response;
            List<String> failedList = new List<String>();

            int tryCount = 0;
            foreach (IWebElement el in links)
            {
                tryCount++;
                try
                {
                    req = (HttpWebRequest)WebRequest.Create(el.GetAttribute("href"));
                    response = (HttpWebResponse)req.GetResponse();
                }
                catch (Exception e)
                {
                    failedList.Add("URL: " + el.GetAttribute("href") + "\n Link Text: " + el.Text + "\n Exception: " + e + "\n-----");
                }

            }

            //checks for failed checks, asserts fail with information if any fails. Prints details to output


            if (failedList.Count > 0)
            {
                foreach (String fail in failedList)
                {
                    Console.WriteLine(fail);
                }
                Assert.Fail(failedList.Count.ToString() + " of " + tryCount.ToString() + " links failed. See output log.");
            }
        }//end of TestMethod1
    }//end of UnitTest1
}//end of namespace
